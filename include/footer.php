<footer>
    <div class="footer_nav">
        <h2>Навигация</h2>
        <?php include "menu.html"?>
    </div>
    <div class="footer_payment">
        <h2>Способы оплаты</h2>
        <img src="" alt="Способы оплаты" title="Способы оплаты">
    </div>
    <div class="footer_contacts">
        <h2>Контакты</h2>
        <ul>
            <li><span>Тел: 8(900)000-00-00</span></li>
            <li><p>Адрес:</p></li>
        </ul>
    </div>
    <div class="copy">
        <span>2016 &copy; Интернет-магазин EmStorm</span>
    </div>
</footer>