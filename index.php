<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>SiteMaket</title>
    <link rel="icon" type="image/x-icon" href="images/favicon.ico">
    <link rel="stylesheet" type="text/css" href="styles/styles.css">
</head>
<body>
<div class="header">
    <div class="mid">
        <?php include "include/header.html"?>
    </div>
</div>
<div class="menu">
    <div class="mid">
        <?php include "include/menu.html"?>
    </div>
</div>
<div class="content clearfix">
    <div class="mid">
        <div class="menu_products">
            <?php include "include/menu_products.html"?>
        </div>
        <div class="center">
            <div class="afisha">
                <img src="images/content/Afisha/sales.jpg">
            </div>
            <div class="hot_products">
                <h1>Горящие товары</h1>
                <div class="blocks">
                    <div class="block">
                        <div class="product">
                            <section>
                                <div class="product_img">
                                    <a href="#"><img src="images/content/Products/Notebook/MacBook.jpg" alt="MacBook Pro" title="MacBook Pro"></a>
                                </div>
                                <div class="product_price">
                                    <h4>Apple MacBook Pro</h4>
                                    <span>105 000руб.</span>
                                    <h2>85 000 руб.</h2>
                                </div>
                                <div class="button_add_basket">
                                    <a href="#">Добавить в корзину</a>
                                </div>
                            </section>
                        </div>
                    </div>
                    <div class="block">
                        <div class="product">
                            <section>
                                <div class="product_img">
                                    <a href="#"><img src="images/content/Products/Notebook/MacBook.jpg" alt="MacBook Pro" title="MacBook Pro"></a>
                                </div>
                                <div class="product_price">
                                    <h4>Apple MacBook Pro</h4>
                                    <span>105 000руб.</span>
                                    <h2>85 000 руб.</h2>
                                </div>
                                <div class="button_add_basket">
                                    <a href="#">Добавить в корзину</a>
                                </div>
                            </section>
                        </div>
                    </div>
                    <div class="block">
                        <div class="product">
                            <section>
                                <div class="product_img">
                                    <a href="#"><img src="images/content/Products/Notebook/MacBook.jpg" alt="MacBook Pro" title="MacBook Pro"></a>
                                </div>
                                <div class="product_price">
                                    <h4>Apple MacBook Pro</h4>
                                    <span>105 000руб.</span>
                                    <h2>85 000 руб.</h2>
                                </div>
                                <div class="button_add_basket">
                                    <a href="#">Добавить в корзину</a>
                                </div>
                            </section>
                        </div>
                    </div>
                    <div class="block">
                        <div class="product">
                            <section>
                                <div class="product_img">
                                    <a href="#"><img src="images/content/Products/Notebook/MacBook.jpg" alt="MacBook Pro" title="MacBook Pro"></a>
                                </div>
                                <div class="product_price">
                                    <h4>Apple MacBook Pro</h4>
                                    <span>105 000руб.</span>
                                    <h2>85 000 руб.</h2>
                                </div>
                                <div class="button_add_basket">
                                    <a href="#">Добавить в корзину</a>
                                </div>
                            </section>
                        </div>
                    </div>
                </div>
            </div>
            <div class="leaders_of_sells">
                <h1>Лидеры продаж</h1>
                <div class="blocks">
                    <div class="block">
                        <div class="product">
                            <section>
                                <div class="product_img">
                                    <a href="#"><img src="images/content/Products/Notebook/MacBook.jpg" alt="MacBook Pro" title="MacBook Pro"></a>
                                </div>
                                <div class="product_price">
                                    <h4>Apple MacBook Pro</h4>
                                    <h2>85 000 руб.</h2>
                                </div>
                                <div class="button_add_basket">
                                    <a href="#">Добавить в корзину</a>
                                </div>
                            </section>
                        </div>
                    </div>
                    <div class="block">
                        <div class="product">
                            <section>
                                <div class="product_img">
                                    <a href="#"><img src="images/content/Products/Notebook/MacBook.jpg" alt="MacBook Pro" title="MacBook Pro"></a>
                                </div>
                                <div class="product_price">
                                    <h4>Apple MacBook Pro</h4>
                                    <h2>85 000 руб.</h2>
                                </div>
                                <div class="button_add_basket">
                                    <a href="#">Добавить в корзину</a>
                                </div>
                            </section>
                        </div>
                    </div>
                    <div class="block">
                        <div class="product">
                            <section>
                                <div class="product_img">
                                    <a href="#"><img src="images/content/Products/Notebook/MacBook.jpg" alt="MacBook Pro" title="MacBook Pro"></a>
                                </div>
                                <div class="product_price">
                                    <h4>Apple MacBook Pro</h4>
                                    <h2>85 000 руб.</h2>
                                </div>
                                <div class="button_add_basket">
                                    <a href="#">Добавить в корзину</a>
                                </div>
                            </section>
                        </div>
                    </div>
                    <div class="block">
                        <div class="product">
                            <section>
                                <div class="product_img">
                                    <a href="#"><img src="images/content/Products/Notebook/MacBook.jpg" alt="MacBook Pro" title="MacBook Pro"></a>
                                </div>
                                <div class="product_price">
                                    <h4>Apple MacBook Pro</h4>
                                    <h2>85 000 руб.</h2>
                                </div>
                                <div class="button_add_basket">
                                    <a href="#">Добавить в корзину</a>
                                </div>
                            </section>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="footer clearfix">
    <div class="mid">
        <?php include "include/footer.php"?>
    </div>
</div>
</body>
</html>